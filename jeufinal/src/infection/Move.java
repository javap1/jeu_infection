package infection;

public class Move
{
protected int posix;	
protected int posiy;	
protected int posax;	
protected int posay;
protected String type;

public Move()
{}
public Move(int posix,int posiy,int posax,int posay,String type)
{
	this.posix=posix;
	this.posiy=posiy;
	this.posax=posax;
	this.posay=posay;
	this.type=type;
}
	public String toString()
	{
		return " Point départ ("+posix+","+posiy+") point d'arrivé ("+posax+","+posay+") type "+type;
	}
	public int getXdepart()
	{
		return posix;
	}
	public void setXdepart(int posix)
	{
		this.posix=posix;
	}
	public int getYdepart()
	{
		return posiy;
	}
	public void setYdepart(int posiy)
	{
		this.posiy=posiy;
	}
	public int getXarrive()
	{
		return posax;
	}
	public void setXarrive(int posax)
	{
		this.posax=posax;
	}
	public int getYarrive()
	{
		return posay;
	}
	public void setYarrive(int posay)
	{
		this.posay=posay;
	}
	public String getType()
	{
		return type;
	}
	public void getType(String type)
	{
		this.type=type;
	}
}
