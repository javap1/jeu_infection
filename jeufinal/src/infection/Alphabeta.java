package infection;
import infection.State;
import java.util.HashSet;
import java.util.*;
public class Alphabeta extends Algorithme
{     
	  //l'implémentation de l'algorithme Alphabeta	  
	  public Alphabeta(String player)
	  {
		  super(player);
	  }
      public float alphabeta(State s,float α,float β,int d)
      {
		  State sj;
		  cpt=cpt+1;
		  //System.out.println("llll");
		  if (d == 0 || s.isOver())
		  {
			  return s.getScore(this.player);
		  }
          else
          {
			 
			 if(s.getCurrentPlayer()==this.player)
			{ 
			 for (Move v: s.getMove(this.player))
			   {  sj=s.play(v);
				  α=Math.max(α,alphabeta(sj,α,β,d-1));
			      if (α>=β)
			      {
					
					  return α;
				  }
			    }
			    return α;
			 }
			 
			 else
			  {
			   for (Move v: s.getMove(s.getCurrentPlayer()))
			   {  sj=s.play(v);
				  β=Math.min(β,alphabeta(sj,α,β,d-1));
			      if(α>=β)
			      {
					  return β;
				  }
			    }
			    return β;
			}
	      }
	  }
	  public Move getBestMove(State s,int d)
	{
		Move bestaction=new Move();
		State nextstate;
		float value;
		float bestvalue=Float.NEGATIVE_INFINITY;
		HashSet <Move> dt=s.getMove(this.player);
		for (Move action: dt)
		{
			
			nextstate=s.play(action);
			value= this.alphabeta(nextstate,Float.NEGATIVE_INFINITY,Float.POSITIVE_INFINITY,d-1);
			if(value>=bestvalue)
			{
				//System.out.println("la profendeur"+d);
				bestvalue=value;
				bestaction=action;
			}
		}
		//System.out.println("le best move get best "+bestaction.toString());
		return bestaction;
	}
      
}
