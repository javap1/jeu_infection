package infection;
import java.util.HashSet;
import java.util.Random;

public class Demo
{
	public static void main(String[] args)
	{
		
		/*// cette partie les joeurs joeuent aléatoirement
		State game=new State();
		int item,i,size;
		String player;
		Move m=new Move();
		HashSet<State> hist= new HashSet<State>();
		while(game.isOver()==false )
		{   
			
			 if(game.contains(game,hist))
		        game.isOver();
			hist.add(game);
			
			player=game.getCurrentPlayer();
			HashSet <Move> dt=game.getMove(player);
			size=dt.size();
			item=new Random().nextInt(size);
		    i=0;
			if(item>=0)
			{
		    for (Move v: dt)
			  {
				if(i==item)
				  m=v;
				i++;
			  }
			
				game=game.play(m);
                game.afficherGrille();
            }                   
				//System.out.println(game.isOver());

		}
	    //la fin du premier teste*/



		//cette partie nous permet de jouer en utilisant soit Minmax soit Alphabeta
		String[] parametres=args;
		String player;
		State game=new State();
		Algorithme aa2=new Minmax("R");
		Algorithme aa1=new Minmax("B");
		Algorithme aa22=new Alphabeta("R");
		Algorithme aa11=new Alphabeta("B");
		HashSet<State> hist=new HashSet<State>();
		while(game.isOver()==false)
		{   
			player=game.getCurrentPlayer();
			if(!game.contains(player,hist))
		    {	
		       hist.add(game);
			   player=game.getCurrentPlayer();
		    if(Boolean.parseBoolean(parametres[2])==false)
			{
				if(player=="B")
				{
					game=game.play(aa1.getBestMove(game,Integer.parseInt(parametres[0])));					
				}
				if(player=="R")
				{
					game=game.play(aa2.getBestMove(game,Integer.parseInt(parametres[1])));
					
				}
			}
			else
			{if(Boolean.parseBoolean(parametres[2])==true)
				{
					
					if(player=="B")
					{
						game=game.play(aa11.getBestMove(game,Integer.parseInt(parametres[0])));
						
					}
					if(player=="R")
					{
						game=game.play(aa22.getBestMove(game,Integer.parseInt(parametres[1])));
					}
					
				}
			}
			game.afficherGrille();
							
            }
            else 
			{
                break;
			}

		}
		//la fin de la partie
		
		//System.out.println("le nombre des noeud pour minmax "+aa1.nombreNoeud());
		//System.out.println("le nombre des noeud pour alphabeta "+aa11.nombreNoeud());
			System.out.println("Game Over ...");
			if(game.getPawnB()>game.getPawnR())
			{
				System.out.println("le joueur bleu qui a gagné avec "+game.getPawnB()+" pions");
			}
			else
			{
				if(game.getPawnB()<game.getPawnR())
				{
					System.out.println("le joueur rouge qui a gagné avec "+game.getPawnR()+" pions");
				}

			}	
			
	}	

 }
