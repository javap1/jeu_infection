package infection;
import infection.State;
import java.util.HashSet;
public class Minmax extends Algorithme
{     
	  
	  //l'implémentation de l'algorithme Minmax
	  public Minmax(String player)
	  {
		  super(player);
	  }
      public float minmax(State s,int d)
      {
		  State sj;
		  float m,b;
		  //System.out.println("la profendeur "+d);
			cpt=cpt+1;
		  if ( d == 0 || s.isOver() )
		  {
			  //System.out.println("la profendeur "+d);
			 return s.getScore(this.player);  
		  }
          else
          {
			  
			 if(s.getCurrentPlayer()==this.player)
			{ b=-100000;
			 for (Move v: s.getMove(this.player))
			   {  sj=s.play(v);
				  //System.out.println("la profendeur "+d);
			      m =this.minmax(sj,d-1);
			     
			      if (b < m)
			      {
					  b=m;
				  }
			        
			    }
			    return b;
			 }
			 
			 else
			{
			   b=+100000;
			   for (Move v: s.getMove(s.getCurrentPlayer()))
			   {  sj=s.play(v);
			      m =this.minmax(sj,d-1);
			      if (b > m)
			       {
					  b=m;
				  }
			    }
			    return b;
			}
			 
	      }
	  }
	  public Move getBestMove(State s,int d)
	{
		Move bestaction=new Move();

		State nextstate;
		float value;
		float bestvalue=(-100000);
		HashSet <Move> dt=s.getMove(this.player);
		for (Move action: dt)
		{
			
			nextstate=s.play(action);
			value=this.minmax(nextstate,d-1);
			//System.out.println("le score:"+this.minmax(nextstate,d-1));
			if(value>= bestvalue)
			{
				bestvalue=value;
				bestaction=action;
			}
		}
		//System.out.println("le score:"+bestvalue);
		nextstate=s.play(bestaction);
		//System.out.println("le best move get best "+bestaction.toString()+" value:"+this.minmax(nextstate,d-1));
		return bestaction;
	}
      
}
