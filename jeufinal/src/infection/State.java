package infection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Random;
public class State
{
	protected String [][] board;
	protected String player;
	protected int turn;
	protected int pawnR;
	protected int pawnB;
	protected boolean hasPassed;
	
	//etat initial 
	public State()
	{
		this.board=new String [7][7];
		for(int i=0;i<7;i++)
		{
			for(int j=0;j<7;j++)
			{
				this.board[i][j]="V";
			}
		}
		this.board[0][0]="R";	
		this.board[0][6]="B";	
		this.board[6][0]="B";	
		this.board[6][6]="R";
		
		this.player="B";
		this.turn=1;
		this.pawnR=2;
		this.pawnB=2;
		this.hasPassed=false;
		
	}
	public State(String [][] board,String player,int turn,int pawnR,int pawnB,boolean hasPassed)
	{
		this.board=board;
		this.player=player;
		this.turn=turn;
		this.pawnR=pawnR;
		this.pawnB=pawnB;
		this.hasPassed=hasPassed;
		
	}
	public int getPawnB()
	{
		return this.pawnB;
	}
	public int getPawnR()
	{
		return this.pawnR;
	}
	//la méthode isOver détermine la fin de la partie
	public boolean isOver()
	{    //System.out.println("test");
		if(pawnR==0 || pawnB==0)
		   return true; 
		if(this.pawnR+this.pawnB==49)
		   return true;
		 		   
	 return false;
	  
	  
	}
	//la méthode retourne le score de joueurs
	public float getScore(String player)
	{
		//System.out.println("pawnB :"+pawnB);
		if(player=="B")
			return (float)this.pawnB/(float)(this.pawnB+this.pawnR);
		if(player=="R")
			return (float)this.pawnR/(float)(this.pawnB+this.pawnR);
		return -1;
	}
	
	//la méthode affiche la grille principale
	public void afficherGrille()
	{
		for(int i=0;i<7;i++)
		{
			for(int j=0;j<7;j++)
			{
				System.out.print(" | "+this.board[i][j]);
			}
			System.out.println(" | ");
		}
		System.out.println("\n");
		System.out.println("player : "+this.player+" tour : "+this.turn+" pion rouge : "+this.pawnR+" pion bleu : "+this.pawnB+" tour passé : "+this.hasPassed);
		
		
	}
	
	
	//la méthode affiche une grille donée pour qu'on fait pas des modifications sur notre grille principale
	public void afficherBoard(String[][] b)
	{
		for(int i=0;i<7;i++)
		{
			for(int j=0;j<7;j++)
			{
				System.out.print(" | "+b[i][j]);
			}
			System.out.println(" | ");
		}
		//System.out.println("player : "+this.player+" tour : "+this.turn+" pion rouge : "+this.pawnR+" pion bleu : "+this.pawnB+" tour passé : "+this.hasPassed);
		
		
	}
	
	//la méthode retourne un tableau des moves possibles d'un joueur
	public HashSet<Move> getMove(String player)
	{
		
		HashSet<Move> setMove=new HashSet<Move>();
		for(int i=0;i<7;i++)
		{
			for(int j=0;j<7;j++)
			{
				if(board[i][j]==player)
				{
					if(i<6)
					{
					 if( board[i+1][j]=="V")
					  {
						Move m=new Move(i,j,i+1,j,"cloner");
						setMove.add(m);
					  }
					 else
					  {if(i<5)
						{  if( board[i+2][j]=="V")	
							{	    
						   Move m=new Move(i,j,i+2,j,"sauter");
						      setMove.add(m);}
					     }
				      }
					 }
					if(j<6)
					{if( board[i][j+1]=="V")
					  {
						Move m=new Move(i,j,i,j+1,"cloner");
						setMove.add(m);
					   }
				     
					  else
					  {if(j<5)
					   {
						   if( board[i][j+2]=="V")	
							{
						   Move m=new Move(i,j,i,j+2,"sauter");
						   setMove.add(m);}
						}   
				      }
				     }
				      if(j<6 && i<6)
					{if( board[i+1][j+1]=="V")
					  {
						Move m=new Move(i,j,i+1,j+1,"cloner");
						setMove.add(m);
					   }
					 else
					  {if(j<5 && i<5)
					   {
						   if( board[i+2][j+2]=="V")	
							{
						   Move m=new Move(i,j,i+2,j+2,"sauter");
						   setMove.add(m);}
					    }
				       }
				      }
				      if(i>0)
					{if( board[i-1][j]=="V")
					  {
						Move m=new Move(i,j,i-1,j,"cloner");
						setMove.add(m);
					   }
					 else
					  {if(i>1)
					     {
							 if( board[i-2][j]=="V")	
							{
						   Move m=new Move(i,j,i-2,j,"sauter");
						   setMove.add(m);}
					     }
				      }
				     }
					 if(i>0 && j<6)
					{if( board[i-1][j+1]=="V")
					  {
						Move m=new Move(i,j,i-1,j+1,"cloner");
						setMove.add(m);
					   }
					 else
					  {if(i>1 && j<5)
					   {
						   if( board[i-2][j+2]=="V")	
							{
						   Move m=new Move(i,j,i-2,j+2,"sauter");
						   setMove.add(m);}
					    }
				       }
				      }
				      
				      if(i>0 && j>0)
					{if( board[i-1][j-1]=="V")
					  {
						Move m=new Move(i,j,i-1,j-1,"cloner");
						setMove.add(m);
					   }
					 else
					  {if(i>1 && j>1)
					   {
						   if( board[i-2][j-2]=="V")	
							{
						   Move m=new Move(i,j,i-2,j-2,"sauter");
						   setMove.add(m);}
				       }
				      }
				     }
				      if(j>0)
					{if( board[i][j-1]=="V")
					  {
						Move m=new Move(i,j,i,j-1,"cloner");
						setMove.add(m);
					   }
					 else
					  {if(j>1)
					     {
							 if( board[i][j-2]=="V")	
							{
						   Move m=new Move(i,j,i,j-2,"sauter");
						   setMove.add(m);}
					     }
				      }
				     }
				      if(i<6 && j>0)
					{if( board[i+1][j-1]=="V")
					  {
						Move m=new Move(i,j,i+1,j-1,"cloner");
						setMove.add(m);
					   }
					 else
					  {if(i<5 && j>1)
					   {
						   if( board[i+2][j-2]=="V")	
							{
						   Move m=new Move(i,j,i+2,j-2,"sauter");
						   setMove.add(m);}
				       }
				      }
				    }
				}
			}
		}
		 return setMove;
	}
	
	// la méthode retourne le joueur qui joue
	public String getCurrentPlayer()
	{
		return this.player;
	}
	
	
	public String[][] board()
	{
		
		String[][] d=new String[7][7];
		for(int i=0;i<7;i++)
		   for(int j=0;j<7;j++)
		      d[i][j]=this.board[i][j];
		
		
		return d;		
		
	}
	//la méthode donne l'état de la partie joué (le joueur joue son role soit cloner soit sauter )
	public State play(Move m)
	{ 
	   //State S=new State(this.board,this.player,this.turn,this.pawnR,this.pawnB,this.hasPassed);
	   State S=new State();
	   S.board=this.board();
       S.player=this.player;
       S.turn=this.turn;
       S.pawnR=this.pawnR;
       S.pawnB=this.pawnB;
       S.hasPassed=this.hasPassed;
	  if(m.type != null)
	  {
		  if(m.getType() == "sauter")
		  {
		  	S.board[m.getXdepart()][m.getYdepart()]="V";
		  	S.board[m.getXarrive()][m.getYarrive()]=S.player;
		  	S.hasPassed=false;
		  	if(S.player=="B")
		  	   S.player="R";
		  	else 
		  	   S.player="B";	
		  	   
		  	S.turn=S.turn+1;
		  }
		  if(m.getType() == "cloner")
		  { 
		  	S.board[m.getXarrive()][m.getYarrive()]=S.player;
		  	S.hasPassed=false;
		  	if(S.player=="B")
		  	{
		  	   S.player="R";
		  	   S.pawnB++;
		  	}
		  	else 
		  	{
		  	   S.player="B";
		  	   S.pawnR++;	
		  	}
		  	 
		  	S.turn=S.turn+1;
		  }
	  }
	  else 
	  {
	  	S.hasPassed=false;
	  	S.turn ++;
	  	if(S.player=="R")
	  	{S.player="B";
		  //System.out.println("lllll"+S.player);
		}
		if(S.player=="B") 
		  S.player="R";	
	  }
	 for (int i=-1;i<=1;i++)
	  {
		  for(int j=-1;j<=1;j++)
		  {
			  if(m.getXarrive()+i>=0 && m.getXarrive()+i<7 && m.getYarrive()+j>=0 && m.getYarrive()+j<7)
			  {
				  if(S.board[m.getXarrive()][m.getYarrive()]=="R" && S.board[m.getXarrive()+i][m.getYarrive()+j]=="B")
				  {
						S.board[m.getXarrive()+i][m.getYarrive()+j]="R";
						S.pawnR++;
						S.pawnB--;
				  }
				  if(S.board[m.getXarrive()][m.getYarrive()]=="B" && S.board[m.getXarrive()+i][m.getYarrive()+j]=="R")
				  {
						S.board[m.getXarrive()+i][m.getYarrive()+j]="B";
						S.pawnB++;
						S.pawnR--;

				  }
			  }
		  }
	  }
		return S;
	}

	public boolean contains(String player,HashSet<State> dt)
	{               int i,j;
			boolean v=false;
			for(State s :dt )
				{
					if(this.player==s.getCurrentPlayer())
						v=true;
						for(i=0;i<7;i++)
						{
							for(j=0;j<7;j++)
							{
								if(this.board[i][j]!=s.board[i][j])
									v=false;
							}
						}
						
						if(v==true)
						  break;
						
					}
				return v;	
       }
		
	
	
	
	
	
	
	

}
