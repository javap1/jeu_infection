package infection;
import java.util.HashSet;

public abstract class Algorithme
{
	//la classe algorithme une classe mère de les classes Alphabeta et Minmax contient une méthode abstact getBestMove qui définit par les deux classes filles
	protected String player;
	protected int d;
	protected int cpt=0;
	public Algorithme(String player)
	{
		this.player=player;
	}
	
	public Algorithme(String player,int d)
	{
		this.player=player;
		this.d=d;
	}
	public int nombreNoeud()
	{
		return cpt;
	}
	public abstract Move getBestMove(State s,int d);
}
